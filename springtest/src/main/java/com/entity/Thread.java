package com.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="thread")
public class Thread {
	@Id
	@GeneratedValue

	private Integer id;

	private String title;

	private String detail;

//	@OneToMany(mappedBy = "thread")
//	@JoinColumn(name="thread_id")
//	private List<Comment> commentList;

//	public List<Comment> getCommentList() {
//		return commentList;
//	}

//	public void setCommentList(List<Comment> commentList) {
//		this.commentList = commentList;
//	}

	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

}
