package com.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
	@Query("select s from User s where s.loginid = :loginid and s.password = :password")
	public User login(@Param("loginid") String loginid, @Param("password") String password);

	public User findByLoginidAndPassword( String loginid,  String password);
}
