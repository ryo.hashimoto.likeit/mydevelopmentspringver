package com.entity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {

	@Query("select s from Comment s where s.threadId = :threadId ")
	public List<Comment> findbyThreadId(@Param("threadId") int threadId);

	@Query("select t,t1 from Comment t,Thread t1 where t.threadId = t1.id")
	public List<Object[]> jointest();

	@Query("select t from Comment t")
	public List<Object[]> findAllByJoin();

}
