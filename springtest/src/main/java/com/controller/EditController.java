package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.entity.Comment;
import com.entity.CommentRepository;
import com.form.commentForm;

@Controller
public class EditController {
	@Autowired
	CommentRepository commentrepo;

	@ModelAttribute("sampleForm")
    public commentForm setupForm() {
		commentForm commentForm = new commentForm();
        return commentForm;
    }

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView helloSpring(@ModelAttribute commentForm commentForm, ModelAndView model) {

		Comment comment = commentrepo.findById(commentForm.getId()).get();

		model.addObject("comment", comment);

		// 画面描画用のテンプレート名を指定
		model.setViewName("edit");
		return model;
	}

	@RequestMapping(value = "/editSubmit", method = RequestMethod.POST)
	public ModelAndView editComment(@ModelAttribute commentForm commentForm,ModelAndView model) {
		Comment comment = commentrepo.findById(commentForm.getId()).get();

		comment.setComment(commentForm.getComment());

		commentrepo.save(comment);

		// 遷移先のパスを指定(一覧表示のパス)
		model.setViewName("redirect:/");
		return model;
	}

}
