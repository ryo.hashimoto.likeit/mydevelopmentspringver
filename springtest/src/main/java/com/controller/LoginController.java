package com.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.entity.User;
import com.entity.UserRepository;
import com.form.UserForm;

@Controller
public class LoginController {
	@Autowired
	UserRepository UserRepo;

	@Autowired
	HttpSession session;

	@ModelAttribute("UserForm")
    public UserForm setupForm() {
		UserForm UserForm = new UserForm();
        return UserForm;
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView login(
			@Validated @ModelAttribute UserForm UserForm,
			BindingResult result,
			ModelAndView model,
			RedirectAttributes attributes
			) {

		// 取得したパラメータをもとに登録用データを作成
		User User = new User();

		String loginid = UserForm.getLoginid();
		String password = UserForm.getPassword();

//		User = UserRepo.login(loginid,password);
//		if(User != null) {
//			session.setAttribute("user", User);
//		}else {
//			System.out.println("インスタンス空");
//		}

		User = UserRepo.findByLoginidAndPassword(loginid,password);
		session.setAttribute("user", User);


		// 遷移先のパスを指定(一覧表示のパス)
		model.setViewName("redirect:/");
		return model;
	}


	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout(
			@Validated @ModelAttribute UserForm UserForm,
			BindingResult result,
			ModelAndView model,
			RedirectAttributes attributes
			) {

		session.invalidate();

		// 遷移先のパスを指定(一覧表示のパス)
		model.setViewName("redirect:/");
		return model;
	}

}
