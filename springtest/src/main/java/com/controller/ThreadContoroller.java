package com.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.entity.Comment;
import com.entity.CommentRepository;
import com.entity.User;
import com.form.commentForm;

@Controller
public class ThreadContoroller {
	@Autowired
	CommentRepository commentrepo;

	@Autowired
	HttpSession session;

	@ModelAttribute("commentForm")
    public commentForm setupForm() {
		commentForm commentForm = new commentForm();
        return commentForm;
    }

	@RequestMapping(value = "/thread", method = RequestMethod.GET)
	public ModelAndView helloSpring(@ModelAttribute commentForm commentForm, ModelAndView model, ServletRequest request) {
		int threadId = Integer.parseInt(request.getParameter("id"));


		List<Comment> commentList = commentrepo.findbyThreadId(threadId);

		model.addObject("commentList", commentList);
		model.addObject("idid", threadId);

		User user = new User();

		if(session.getAttribute("user") != null) {
			user = (User)session.getAttribute("user");
		}

		model.addObject("user", user);

		// 画面描画用のテンプレート名を指定
		model.setViewName("thread");
		return model;
	}

	@RequestMapping(value = "/submit", method = RequestMethod.GET)
	public ModelAndView submitComment(ModelAndView model) {
		// 遷移先のパスを指定(一覧表示のパス)
		model.setViewName("redirect:/thread");
		return model;
	}

	@RequestMapping(value = "/submit", method = RequestMethod.POST)
	public ModelAndView submitComment(
			@Validated @ModelAttribute commentForm commentForm,
			BindingResult result,
			ModelAndView model,
			RedirectAttributes attributes,
			ServletRequest request
			) {

		if (result.hasErrors()) {
			return helloSpring(commentForm, model,request);
		}

		// 取得したパラメータをもとに登録用データを作成
		Comment comments = new Comment();
		comments.setName(commentForm.getName());
		comments.setComment(commentForm.getComment());
		comments.setThreadId(commentForm.getThreadId());
		comments.setSubmitDate(new Date());

		// 登録
		commentrepo.save(comments);

		// 遷移先のパスを指定(一覧表示のパス)
		model.setViewName("redirect:/");
		return model;
	}

	@RequestMapping("/delete")
	public ModelAndView deleteTask(@ModelAttribute commentForm commentForm, ServletRequest request, ModelAndView model) {
		// 削除処理
		commentrepo.deleteById(commentForm.getId());

		// 遷移先のパスを指定(一覧表示のパス)
		model.setViewName("redirect:/thread");
		return model;
	}
}
