package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.entity.User;
import com.entity.UserRepository;
import com.form.UserForm;

@Controller
public class RegistController {
	@Autowired
	UserRepository UserRepo;

	@ModelAttribute("UserForm")
    public UserForm setupForm() {
		UserForm UserForm = new UserForm();
        return UserForm;
	}


	@RequestMapping(value = "/regist", method = RequestMethod.POST)
	public ModelAndView regist(
			@Validated @ModelAttribute UserForm UserForm,
			BindingResult result,
			ModelAndView model,
			RedirectAttributes attributes
			) {

		// 取得したパラメータをもとに登録用データを作成
		User User = new User();

		User.setLoginid(UserForm.getLoginid());
		User.setPassword(UserForm.getPassword());
		User.setName(UserForm.getName());

		// 登録
		UserRepo.save(User);

		// 遷移先のパスを指定(一覧表示のパス)
		model.setViewName("redirect:/");
		return model;
	}
}
