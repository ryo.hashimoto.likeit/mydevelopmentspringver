package com.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.entity.Comment;
import com.entity.CommentRepository;
import com.entity.Thread;
import com.entity.ThreadRepository;
import com.entity.User;
import com.form.ThreadForm;

@Controller
public class IndexController {
	@Autowired
	ThreadRepository threadRepo;

	@Autowired
	CommentRepository commentrepo;

	@Autowired
	HttpSession session;

	@ModelAttribute("ThreadForm")
    public ThreadForm setupForm() {
		ThreadForm ThreadForm = new ThreadForm();
        return ThreadForm;
    }

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView helloSpring(@ModelAttribute ThreadForm ThreadForm, ModelAndView model) {

		List<Thread> threadList = threadRepo.findAll();

		model.addObject("threadList", threadList);

		List<Comment> commentListAll =  commentrepo.findAll();

		model.addObject("commentListAll", commentListAll);

		User user = new User();

		if(session.getAttribute("user") != null) {
			user = (User)session.getAttribute("user");
		}

		model.addObject("user", user);



		// 画面描画用のテンプレート名を指定
		model.setViewName("index");
		return model;
	}

	@RequestMapping(value = "/createThread", method = RequestMethod.GET)
	public ModelAndView submitComment(ModelAndView model) {
		// 遷移先のパスを指定(一覧表示のパス)
		model.setViewName("redirect:/");
		return model;
	}

	@RequestMapping(value = "/createThread", method = RequestMethod.POST)
	public ModelAndView submitComment(
			@Validated @ModelAttribute ThreadForm ThreadForm,
			BindingResult result,
			ModelAndView model,
			RedirectAttributes attributes
			) {

		// 取得したパラメータをもとに登録用データを作成
		Thread thread = new Thread();


		thread.setTitle(ThreadForm.getTitle());
		thread.setDetail(ThreadForm.getDetail());


		// 登録
		threadRepo.save(thread);

		// 遷移先のパスを指定(一覧表示のパス)
		model.setViewName("redirect:/");
		return model;
	}

}
